# Gijirockn
Bitbucketでも採用されている[Creole記法](http://wikicreole.org/)を使って  
リアルタイムに議事録を共有する事ができるサービスです。

議事録をプロジェクターに映しながらMTGする事ってあると思うんですが、リモートだとなかなか難しいですよね。

そこでこのGijirock'nを使う事で、リアルタイムに議事録を共有できます。

1文字タイプする度にその議事録を閲覧してる全ーザへ送信され、Creole記法からHTMLにパースして表示されます。

[Creole記法の書き方についてはこちらをご覧ください](http://takyam.com/categories/bitbucket/bitbucket-wiki-cheat-sheet)

## Demo
* [http://gijirockn.takyam.com:3000/](http://gijirockn.takyam.com:3000/)

## 使い方
1. ログインする
1. 議事録を作る
1. 議事録のURLをメンバーにシェアする
1. MTGしながら議事録を書く
1. 議事録をBitbucketのwikiにコピペする

## 動作ブラウザ
* Chrome(PC)
* Firefox(PC/Android)
* Android標準ブラウザ

## License
	The MIT License (MIT)  
	Copyright (c) 2012 takyam <takayuki.yamaguch.m@gmail.com>
	
	Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
	
	The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
	
	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

## Installation
* $ cd /path/to/install
* $ git clone https://takyam@bitbucket.org/takyam/gijirockn.git
* $ npm install -g git://github.com/socketstream/socketstream.git
* $ npm link socketstream
* $ npm install
* $ cp config/default.coffee.sample config/default.coffee
* Input your Facebook app ID and secret key to config/default.coffee
* $ npm install -g forever
* run mongodb
* run redis
* $ SS_ENV=production forever start app.js
* Access http://your.domain.com:3000/ on browser.

## 動作環境
* NodeJS v0.8.x
* SocketStream 0.3.0 RC2
* Redis 2.4.x
* MongoDB 2.0.x

## Author
* takyam
 * [Twitter](https://twitter.com/takyam)
 * [Facebook](https://www.facebook.com/takyam1213)
 * [blog](http://new.takyam.com/)
