exports.actions = (req, res, ss) ->
  req.use('session')
  Gijirockn = require('./models/gijirockn').Gijirockn
  
  isLoggedIn: ->
    isLoggedIn = req.session?.userId?.length? and req.session.userId.length > 0
    res isLoggedIn
  
  getHash: ->
    hash = ''
    if req.session?.gijirockn_hash?.length? and req.session.gijirockn_hash.length > 0
      hash = req.session.gijirockn_hash
    res hash
  
  getUserGijirockns: ->
    if req.session?.userId?.length? and req.session.userId.length > 0
      Gijirockn.find {userId: req.session.userId}, (err, gijirockns)->
        res gijirockns
    else
      res []
  
  getNewGijirockn: ->
    if req.session?.userId?.length? and req.session.userId.length > 0
      gijirockn = new Gijirockn
        userId: req.session.userId
        text: ''
        linkHash: ''
      gijirockn.save (err, obj)->
        res obj._id
    else
      res null

  getGijirockn: (hash) ->
    if hash?.length? and hash.length > 0
      Gijirockn.findById hash, (err, data) ->
        if err is null and data isnt null
          subscribeId = data._id.toString()
          req.session.subscribeId = subscribeId
          req.session.channel.reset()
          req.session.channel.subscribe(subscribeId)
          req.session.save()
          res {
            id: data._id
            isEditer: (req?.session?.userId? and data.userId is req.session.userId)
            text: data.text
            lastUpdateAt: data.lastUpdateAt
            createdAt: data.lastUpdateAt
          }
        else
          res false
    else
      res false
  
  removeGijirockn: (hash) ->
    if hash?.length? and hash.length > 0
      Gijirockn.findById hash, (err, data) ->
        if err is null and data isnt null and data.userId is req.session.userId
          data.remove()
          #data.save()
    
    res true

  updateText: (obj) ->
    id = req.session.subscribeId
    text = ''
    if obj?.text?.length? and obj.text.length >= 0
      text = obj.text
    if text?.length? && text.length >= 0 && id?.length? && id.length > 0 
      ss.publish.channel(id, 'updateText', {text: text})
    if obj?.id?.length? and obj.id.length > 0 and obj?.text?.length? and obj.text.length >= 0
      Gijirockn.findById obj.id, (err, data) ->
        if err is null and data.userId is req.session.userId
          data.text = obj.text
          data.save()
    res true
  
  logout: ->
    delete req.session.userId
    delete req.session.subscribeId
    req.session.channel.reset()
    req.session.save()
    res true
