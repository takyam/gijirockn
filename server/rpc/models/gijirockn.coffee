mongoose = require 'mongoose'
Schema = mongoose.Schema

GijirocknSchema = new Schema
  userId: String
  text: String
  linkHash: String
  createdAt: {type: Date, default: Date.now}
  lastUpdateAt: {type: Date, default: Date.now}

GijirocknSchema.pre 'save', (next)->
  this.lastUpdateAt = (new Date()).getTime()
  next()

mongoose.model 'Gijirockn', GijirocknSchema
mongoose.connect 'mongodb://localhost/Gijirockn'

module.exports.Gijirockn = mongoose.model 'Gijirockn'
module.exports.GijirocknSchema = GijirocknSchema
