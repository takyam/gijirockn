http = require 'http'
ss = require 'socketstream'
everyauth = require 'everyauth'
fbConfig = require('config').Facebook

#Define a single-page client
ss.client.define 'main',
  view: 'app.html',
  css: ['app.less'],
  code: ['libs/jquery.min.js', 'libs/bootstrap.min.js', 'libs/jscreole/creole.js', 'app'],
  tmpl: '*'

#Serve this client on the root URL
ss.http.route '/', (req, res) ->
  if req?.session?
    req.session.gijirockn_hash = null
    if req.url?.length? and req.url.length > 0
      url = req.url.replace(/#.*/, '').replace(/^\//, '')
      params = url.split('/')
      if params.length > 0 and params[0].length > 0
        req.session.gijirockn_hash = params[0]
    req.session.save()
    console.log req.session
  res.serveClient 'main'

#セッションはRedisを利用。httpとwsで共有するため
ss.session.store.use 'redis'

#Facebookログイン
everyauth.facebook
  .appId(fbConfig.appID)
  .appSecret(fbConfig.secretKey)
  .myHostname('http://gijirockn.takyam.com')
  .findOrCreateUser((session, accessToken, accessTokenExtra, facebookUserMetadata) ->
    session.userId = facebookUserMetadata.id
    session.save()
    return true
  ).redirectPath('/')
everyauth.facebook.moduleErrback( (err) ->
  res.serve('/')
)

ss.http.middleware.prepend ss.http.connect.bodyParser()
ss.http.middleware.append everyauth.middleware()

#Code Formatters
ss.client.formatters.add require('ss-coffee')
ss.client.formatters.add require('ss-stylus')
ss.client.formatters.add require('ss-less')

#Use server-side compiled Hogan (Mustache) templates. Others engines available
ss.client.templateEngine.use require('ss-hogan')

#Minimize and pack assets if you type: SS_ENV=production node app.js
ss.client.packAssets() if ss.env is 'production'

#Start web server
server = http.Server ss.http.middleware
server.listen 3000

#Start SocketStream
ss.start server
