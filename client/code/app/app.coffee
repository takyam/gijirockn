$window = $(window)
$editor = $([])
$view = $([])
creole = new Parse.Simple.Creole {
  forIE: document.all
}
ss.event.on 'updateText', (obj) ->
  text = obj.text
  if $view.length > 0 and text?.length? and text.length >= 0
    $view.empty()
    creole.parse $view[0], text
formatDate = (date) ->
  date = new Date(date)
  yy = date.getYear()
  mm = date.getMonth() + 1
  dd = date.getDate()
  hh = date.getHours()
  ii = date.getMinutes()
  ss = date.getSeconds()
  yy += 1900 if yy < 2000
  mm = '0' + mm if mm < 10
  dd = '0' + dd if dd < 10
  hh = '0' + hh if hh < 10
  ii = '0' + ii if ii < 10
  ss = '0' + ss if ss < 10
  return yy + '/' + mm + '/' + dd + ' ' + hh + ':' + ii + ':' + ss

origin = location.origin + '/'

$homelink = $('#homelink')
$homelink.off('click').on('click', ->
  openIndex()
  return false
)

$logoutContainer = $('#logout-container')

$container = $('#container')
ss.rpc 'app.getHash', (hash) ->
  if hash?.length? and hash.length > 0
    openGijirockn(hash)
  else
    openIndex()

$errbox = $('#errorbox')
addError = (string) ->
  $errbox.html ss.tmpl['app-error'].render({error: string})

openIndex = ->
  ss.rpc 'app.isLoggedIn', (isLoggedIn) ->
    $logoutContainer.empty()
    $container.empty()
    if isLoggedIn
      $logoutContainer.html ss.tmpl['app-logout-btn'].render()
      $logoutContainer.find('#logout-btn').off('click').on('click', ->
        ss.rpc 'app.logout', ->
          openIndex()
        return false
      )
      
      menuHtml = ss.tmpl['app-login-menu'].render()
      $container.prepend($(menuHtml))
      ss.rpc 'app.getUserGijirockns', (gijirockns) ->
        list = []
        for gijirockn in gijirockns
          title = gijirockn.text
          if title.length > 0
            title = title.replace(/(\r\n|\r|\n).*/g, '')
          else
            title = '( empty no title )'
          list.push {
            id: gijirockn._id
            title: title
            lastUpdateAt: gijirockn.lastUpdateAt.toString().replace(/^([0-9]{4})-([0-9]{2})-([0-9]{2})T([0-9]{2}):([0-9]{2}):([0-9]{2}).*$/, '$1/$2/$3 $4:$5:$6') #formatDate(gijirockn.lastUpdateAt)
          }
        
        tableHtml = ss.tmpl['app-list'].render {gijirockns: list}
        $container.append $(tableHtml)
        
        $links = $('a.openGijirockn')
        $links.off('click').on 'click', ->
          $link = $(this)
          openGijirockn($link.attr('data-hash'))
          return false
        $('a.remove-btn').off('click').on('click', ->
          ss.rpc 'app.removeGijirockn', $(this).attr('data-hash'), (result) ->
            openIndex()
          return false
        )
    else
      $logoutContainer.html ss.tmpl['app-login-btn'].render()
      menuHtml = ss.tmpl['app-not-login-menu'].render()
      $container.prepend($(menuHtml))
      $editorSample = $('#editor-sample')
      $viewSample = $('#html-view-sample')
      creole.parse $viewSample[0], $editorSample.val()
      $editorSample.off('keydown').on('keydown', ->
        $viewSample.empty()
        creole.parse $viewSample[0], $editorSample.val()
      )
    $btnLogin = $('#btnLogin')
    $btnNewGijirockn = $('#btnNewGijirockn')
    
    $btnNewGijirockn.on 'click', ->
      if $btnNewGijirockn.attr('disabled') is 'disabled'
        return false
      
      ss.rpc 'app.getNewGijirockn', (newHash) ->
        openGijirockn(newHash)
      
      return false

openGijirockn = (hash) ->
  ss.rpc 'app.getGijirockn', hash, (gijirockn) ->
    if gijirockn? and gijirockn isnt false and gijirockn isnt null
      gijirockn.origin = origin
      if gijirockn?.isEditer? and gijirockn.isEditer is true
        $container.html ss.tmpl['app-edit'].render(gijirockn)
        $editor = $('textarea#editor')
        $editor.height(parseInt(($window.height() - 100) * 0.95))
        $editor.off('keyup').on('keyup', ->
          ss.rpc('app.updateText', {id: gijirockn.id, text: $editor.val()}, (result)->
          )
        )
      else
        $container.html ss.tmpl['app-view'].render(gijirockn)
      $view = $('div#html-view')
      if $view.length > 0
        $view.empty()
        creole.parse $view[0], gijirockn.text
    else
      openIndex()
      addError '存在しない議事録が指定されています'